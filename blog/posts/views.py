from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404
from .forms import PostForm, AddCategoryForm, AddCommentForm
from .models import *


# Create your views here.

def home(request):
    queryset_list = Post.objects.all()
    category_list = Category.objects.all()
    paginator = Paginator(queryset_list, 2)  # Show 25 contacts per page
    user = request.user
    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    context = {
        "object_list": queryset,
        "title": "List",
        "category_list": category_list,
        "user": user,
    }

    return render(request, "index.html", context)


def post_detail(request, pk=None):
    post = get_object_or_404(Post, id=pk)
    comment = Comment.objects.filter(post_id=pk)
    form = AddCommentForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.post = post
        instance.save()
        return redirect('detail', pk=post.pk)
    context = {
        "title": post.title,
        "instance": post,
        "comment": comment,
        "form": form,
    }
    return render(request, "details.html", context)


def post_form(request):
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    if not request.user.is_authenticated():
        raise Http404
    form = PostForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "form": form,
    }

    return render(request, "form.html", context)


def post_edit(request, pk=None):
    instance = get_object_or_404(Post, id=pk)
    form = PostForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "saved")
        return HttpResponseRedirect(instance.get_absolute_url())
    else:
        messages.error(request, "not saved")
    context = {
        "title": instance.title,
        "instance": instance,
        "form": form,
    }

    return render(request, "form.html", context)


def post_delete(request, pk=None):
    instance = get_object_or_404(Post, id=pk)
    instance.delete()
    messages.success(request, "deleted")
    return redirect("list")


def filter_post(request, pk=None):
    instance = get_object_or_404(Category, id=pk)
    query_list = Post.objects.filter(category__name__exact=instance.name)
    category_list = Category.objects.all()
    paginator = Paginator(query_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        queryset = paginator.page(paginator.num_pages)
    context = {
        "object_list": queryset,
        "title": "List",
        "category_list": category_list,
    }

    return render(request, "index.html", context)


def add_category(request):
    form = AddCategoryForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('add-form')
    return render(request, "form.html", {
        "form": form
    })
