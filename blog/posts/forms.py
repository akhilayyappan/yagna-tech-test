from django import forms
from .models import *


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            "title",
            "content",
            "category",
        ]


class AddCategoryForm(forms.ModelForm):
    class Meta:
        model = Category

        fields = [
            'name',
        ]


class AddCommentForm(forms.ModelForm):
    class Meta:
        model = Comment

        fields = [
            'author',
            'text',
        ]
