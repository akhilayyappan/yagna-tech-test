from django.conf.urls import url
from django.contrib import admin
import posts.views

urlpatterns = [
    url(r'^$', posts.views.home, name='list'),
    url(r'^form/$', posts.views.post_form, name='add-form'),
    url(r'^add-category/', posts.views.add_category, name='add-category'),
    url(r'^detail/(?P<pk>\d+)/$', posts.views.post_detail , name='detail'),
    url(r'^(?P<pk>\d+)/edit/$', posts.views.post_edit, name='edit'),
    url(r'^(?P<pk>\d+)/filter-post/$', posts.views.filter_post, name='filter-post'),
    url(r'^(?P<pk>\d+)/delete/$', posts.views.post_delete, name='delete'),
]
